# MPM-Rent
MPM-Rent
 
### Github workflow

```
1. Create new issue in Github (Based on Trello).
2. Create a title and description on issue according to the task.
3. In your local, checkout to staging.
4. Pull your staging based on github remote to get the latest commit.
5. Working the issue on staging branch.
6. Continue your work, don't forget to commit and push regularly to your remote branch on staging.
7. Create APK and debug on device and give it to the tester for getting some reviews/feedbacks branch on staging.
8. After you think your work is done, push and merge to finishing branch.
9. You should request the review for your codes to Agung Trilaksono SP to make sure your work goes through the our standard codes.
10. If your work failed in review, continue your work, comment the revision.
11. If your work passed the review, you can merge your branch to the master branch.
12. After the tester and you think the work is done, take a log version and review the APK to Yahya.
13. After Yahya is done, push and merge to master branch.
14. Upload the latest APK file based on master branch to play store.
15. Take A note on play store about your log version and changes description.
16. That's it!
```

### Branch
Development = For internal development<br />
Staging = For debug and ready to release<br />
Finishing = For internal UAT<br /><br />
UAT = For Testing from Client<br /><br />
Master = For Production<br /><br />

Development -> Staging -> Finishing -> UAT -> Master <br /><br />

### Commit convention

User prefix [{Module}][{Feature}] : {Description}

:hammer: `:hammer:` Use this emoji for every new feature or component.

:wrench: `:wrench:` This emoji for fixing or update your work.

:sparkles: `:sparkles:` Use this emoji if you had finishing new feature or component.

:construction: `:construction:` If you can't finish your work, or you get a blocker and cannot continuing your work, use this emoji for the last commit.


## Resources
Laravel <br>
OctoberCMS
