// $(document).ready(function () {

//     window.onscroll = function () { myFunction() };

//     var navbar = document.getElementById("navbar");
//     var sticky = navbar.offsetTop;

//     function myFunction() {
//      if (window.pageYOffset >= sticky) {
//         navbar.classList.add("sticky")
//      } else {
//         navbar.classList.remove("sticky");
//      }
//     }

// });


var hash = document.location.hash;
var prefix = "tab_";
$(document).on('click', '.tab_link', function(){
var hash2 = document.location.hash;
var hash3 = $(this).data('tab-id');
$('.nav-tabs a[href="'+hash3.replace(prefix,"")+'"]').tab('show');
})
// Javascript to enable link to tab
if (hash) {
console.log(hash);
  $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
} 

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
  window.location.hash = e.target.hash.replace("#", "#" + prefix);
});
        
$(document).on('click','.tab-link-rent', function(){
    var current_tab = $(this).data('item');
    if(current_tab)
    {
        $('.tab-item-rent').addClass('hidden');
        $('.tab-list-rent').addClass('hidden');
    }
    if(current_tab == 'luxury')
    {
        $('#detail-luxury').removeClass('hidden');
    }
    else if ( current_tab == 'passenger' )
    {
        $('#detail-passenger').removeClass('hidden');
    }
    else if ( current_tab == 'logistic' )
    {
        $('#detail-logistic').removeClass('hidden');
    }
    else if ( current_tab == 'heavyduty' )
    {
        $('#detail-heavyduty').removeClass('hidden');
    }
})

$(document).on('click', '.back-to-list-rent, .btn-back-desc', function(event) {
    $('.tab-item-rent').addClass('hidden');
    $('.tab-list-rent').removeClass('hidden');
});

$('#owl-index').owlCarousel({
    center: true,
    items: 1,
    loop: true,
    dots: true,
    nav: false,
    navText : ["",""],
    responsive:{
        800:{
            items:1
        }
    }
});
$('#owl-pages').owlCarousel({
    center: true,
    items: 1,
    loop: true,
    dots: true,
    nav: false,
    navText : ["",""],
    responsive:{
        800:{
            items:1
        }
    }
});

$('#owl-career').owlCarousel({
    autoplay:true,
    autoplayTimeout:5000,
    items: 1,
    loop: true,
    margin:10,
    dots: false,
    nav: true,
    navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
});

$('#owl-testi').owlCarousel({
    items: 4,
    loop: true,
    autoHeight: false,
    margin:20,
    dots: true,
    nav: true,
    navText : ["",""],
    responsive:{
        0:{
            items:1
        },
        700:{
            items:4
        },
    }
});
$('#owl-type').owlCarousel({
    items: 4,
    loop: true,
    margin:30,
    dots: true,
    nav: false,
    navText : ["",""],
    responsive:{
        0:{
            items:1
        },
        400:{
            items:2
        },
        600:{
            items:3
        },
        800:{
            items:4
        }
    }
});

$('#owl-type-detail-luxury, #owl-type-detail-passenger').owlCarousel({
    items: 3,
    loop: false,
    margin:30,
    dots: true,
    nav: false,
    navText : ["",""],
    responsive:{
        0:{
            items:1
        },
        400:{
            items:2
        },
        600:{
            items:3
        },
        800:{
            items:4
        }
    }
});



function readFile(input) {
 if (input.files && input.files[0]) {
 var reader = new FileReader();
 
 reader.onload = function (e) {
 var htmlPreview = 
 '<img width="200" src="' + e.target.result + '" />'+
 '<p>' + input.files[0].name + '</p>';
 var wrapperZone = $(input).parent();
 var previewZone = $(input).parent().parent().find('.preview-zone');
 var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');
 
 wrapperZone.removeClass('dragover');
 previewZone.removeClass('hidden');
 boxZone.empty();
 boxZone.append(htmlPreview);
 };
 
 reader.readAsDataURL(input.files[0]);
 }
}
function reset(e) {
 e.wrap('<form>').closest('form').get(0).reset();
 e.unwrap();
}
$(".dropzone").change(function(){
 readFile(this);
});
$('.dropzone-wrapper').on('dragover', function(e) {
 e.preventDefault();
 e.stopPropagation();
 $(this).addClass('dragover');
});
$('.dropzone-wrapper').on('dragleave', function(e) {
 e.preventDefault();
 e.stopPropagation();
 $(this).removeClass('dragover');
});
$('.remove-preview').on('click', function() {
 var boxZone = $(this).parents('.preview-zone').find('.box-body');
 var previewZone = $(this).parents('.preview-zone');
 var dropzone = $(this).parents('.form-group').find('.dropzone');
 boxZone.empty();
 previewZone.addClass('hidden');
 reset(dropzone);
});
