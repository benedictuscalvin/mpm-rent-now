<?php

namespace Deptech\SeoExtension\Components;

class SeoNews extends \Cms\Classes\ComponentBase
{

	public $page;
    public $seo_title;
    public $seo_description;
    public $seo_keywords;
    public $canonical_url;
    public $redirect_url;
    public $robot_index;
    public $robot_follow;
	public function componentDetails()
    {
        return [
            'name'        => 'SEO News',
            'description' => 'SEO for news'
        ];
    }


    public function onRun()
    {

    }
}
