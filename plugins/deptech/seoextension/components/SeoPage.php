<?php namespace Deptech\SeoExtension\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Request;
use Deptech\SeoExtension\models\Settings;
use URL;
use Crypt;
use Deptech\MpmRent\Models\NewsModel;
// use Deptech\Main\Models\Brochures;
use Deptech\SeoExtension\Helpers\General as Helper;
use RainLab\Translate\Classes\Translator;

class SeoPage extends ComponentBase{
	public $page;
	public $seo_title;
	public $seo_description;
	public $seo_keywords;
	public $canonical_url;
	public $redirect_url;
	public $robot_index;
	public $robot_follow;
	public $hasBlog;

	public $ogTitle;
	public $ogUrl;
	public $ogDescription;
	public $ogSiteName;
	public $ogFbAppId;
	public $ogLocale;
	public $ogImage;

	public $twitterCard;
	public $twitterTitle;
	public $twitterDescription;
	public $twitterUrl;
	public $twitterImage;

	public $settings;

	public function componentDetails(){
		return [
			'name'        => 'SEO Page',
			'description' => 'Inject SEO Fields of CMS pages'
		];
	}

	public function defineProperties(){
		return [
		];
	}

	public function onRun(){
		$theme = Theme::getActiveTheme();
		$page = Page::load($theme,$this->page->baseFileName);
		$this->page["hasBlog"] = false;
		$translator = Translator::instance();
		$activeLocale = $translator->getLocale();

		$this->settings = $this->page["settings"] = Settings::instance();
		$settings = Settings::instance();
		$helper = new Helper();
		$currentPage = ($this->page->param('page') == false) ? 1 : $this->page->param('page');
		$pagingText = ($activeLocale == 'id') ? 'Hal. ' : 'Page ';
		$pagingText .= $currentPage;
		$title = ($this->page->id === 'news') ? $helper->generateTitle($this->page->title).' - '.$pagingText : $helper->generateTitle($this->page->title);
		if (!$page->hasComponent("SeoPostPage")) {
			$metaDescription = ($this->page->id === 'news') ? $this->page->meta_description.' - '.$pagingText : $this->page->meta_description;
			$metaTitle = ($this->page->id === 'news') ? $this->page->meta_title.' - '.$pagingText : $this->page->meta_title;
			$this->seo_title = $this->page["seo_title"] = empty($this->page->meta_title) ? $title : $metaTitle;
			$this->seo_description = $this->page["seo_description"] = $metaDescription;
			$this->seo_keywords = $this->page["seo_keywords"] = $this->page->seo_keywords;
			$this->canonical_url = $this->page["canonical_url"] = $this->page->canonical_url;
			$this->redirect_url = $this->page["redirect_url"] = $this->page->redirect_url;
			$this->robot_follow = $this->page["robot_follow"] = $this->page->robot_follow;
			$this->robot_index = $this->page["robot_index"] = $this->page->robot_index;

			if ($settings->enable_og_tags) {
				$this->ogTitle = empty($this->page->meta_title) ? $title : $this->page->meta_title;
				$this->ogDescription = $this->page->meta_description;
				$this->ogUrl = empty($this->page->canonical_url) ? Request::url() : $this->page->canonical_url ;
				$this->ogSiteName = $settings->og_sitename;
				$this->ogImage = $this->page->image;
				$this->ogFbAppId = $settings->og_fb_appid;
			}

			if ($settings->enable_twitter_tags) {
				$this->twitterCard = $settings->twitter_card;
				$this->twitterTitle = empty($this->page->meta_title) ? $title : $this->page->meta_title;
				$this->twitterDescription = $this->page->meta_description;
				$this->twitterUrl = empty($this->page->canonical_url) ? Request::url() : $this->page->canonical_url ;
				$this->twitterImage = $this->page->image;
			}
		} else {
			$this->hasBlog = $this->page["hasBlog"] = true;

			$slug = $this->param('slug');
			$post = '';
			$post = NewsModel::where('slug', $slug)->get()->first();


			if ($post) {
				$this->seo_title = $this->page["seo_title"] = empty($post->seo_title) ? $helper->generateTitle($post->lang($activeLocale)->title) : $helper->generateTitle($post->lang($activeLocale)->seo_title);
				$this->seo_description = $this->page["seo_description"] = $post->lang($activeLocale)->seo_description;
				$this->seo_keywords = $this->page["seo_keywords"] = $post->seo_keywords;
				$this->canonical_url = $this->page["canonical_url"] = empty($post->canonical_url) ? Request::url() : $post->canonical_url;
				$this->redirect_url = $this->page["redirect_url"] = $post->redirect_url;
				$this->robot_follow = $this->page["robot_follow"] = $post->robot_follow;
				$this->robot_index = $this->page["robot_index"] = $post->robot_index;
				$settings = Settings::instance();

				if($settings->enable_og_tags){
					$this->ogTitle = $post->seo_title;
					$this->ogDescription = $post->seo_description;
					$this->ogUrl = $post->canonical_url;
					$this->ogSiteName = $settings->og_sitename;
					$this->ogImage = $post->image;
					$this->ogFbAppId = $settings->og_fb_appid;
				}

				if($settings->enable_twitter_tags){
					$this->twitterCard = $settings->twitter_card;
					$this->twitterTitle = $post->seo_title;
					$this->twitterDescription = $this->page->meta_description;
					$this->twitterUrl = empty($post->canonical_url) ? Request::url() : $post->canonical_url;
					$this->twitterImage = $post->image;
				}
			}
		}
	}
}