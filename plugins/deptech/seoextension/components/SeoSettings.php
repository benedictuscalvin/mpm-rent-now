<?php

namespace Deptech\SeoExtension\Components;
use Lang;
use Redirect;
use BackendAuth;
use Request;
use Cms\Classes\Page;
use Cms\Classes\Theme;

use Deptech\SeoExtension\Models\Settings as SeoSettingsModel;

class SeoSettings extends \Cms\Classes\ComponentBase
{

    public $page;
    public $seo_title;
    public $seo_description;
    public $seo_keywords;
    public $canonical_url;
    public $redirect_url;
    public $robot_index;
    public $robot_follow;
    public $hasNews;
    public $hasStatic;

    public $ogTitle;
    public $ogUrl;
    public $ogDescription;
    public $ogSiteName;
    public $ogFbAppId;
    public $ogLocale;
    public $ogImage;

    public $twitterTitle;
    public $twitterDescription;
    public $twitterUrl;
    public $twitterSiteName;
    public $twitterCard;

	public function componentDetails()
    {
        return [
            'name'        => 'Seo Settings',
            'description' => 'Include Meta tags settings'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() 
    {
    	$theme = Theme::getActiveTheme();
        $page = Page::load($theme,$this->page->baseFileName);
        $this->page["hasNews"] = false;
        $this->page["hasStatic"] = false;

        if($page->hasComponent("seo_news"))
        {
            $this->hasNews = $this->page["hasNews"] = true;
        }
        else if ($page->hasComponent("seo_static"))
        {
            $this->hasStatic = $this->page["hasStatic"] = true;
        }
        else 
        {
            $settings = SeoSettingsModel::instance();

            $this->seo_title       = $this->page["seo_title"]       = empty($this->page->meta_title) ? $this->page->title : $this->page->meta_title;
            $this->seo_description = $this->page["seo_description"] = empty($this->page->meta_description) ? $settings->description : $this->page->meta_description;
            $this->seo_keywords    = $this->page["seo_keywords"]    = empty($this->page->seo_keywords) ? $settings->keywords : $this->page->seo_keywords;
            $this->canonical_url   = $this->page["canonical_url"]   = $this->page->canonical_url;
            $this->redirect_url    = $this->page["redirect_url"]    = $this->page->redirect_url;
            $this->robot_follow    = $this->page["robot_follow"]    = $this->page->robot_follow;
            $this->robot_index     = $this->page["robot_index"]     = $this->page->robot_index;


            if($settings->enable_og_tags)
            {
                $this->ogTitle          = $this->page['ogTitle']       = empty($this->page->meta_title) ? $this->page->title : $this->page->meta_title;
                $this->ogDescription    = $this->page['ogDescription'] = $this->page->meta_description;
                $this->ogUrl            = $this->page['ogUrl']         = empty($this->page->canonical_url) ? Request::url() : $this->page->canonical_url ;
                $this->ogSiteName       = $this->page['ogSiteName']    = $settings->og_sitename;
                $this->ogFbAppId        = $this->page['ogFbAppId']     = $settings->og_fb_appid;
            }

            if($settings->enable_twitter_tags)
            {
                $this->twitterTitle       = $this->page['twitterTitle']       = empty($this->page->meta_title) ? $this->page->title : $this->page->meta_title;
                $this->twitterDescription = $this->page['twitterDescription'] = $this->page->meta_description;
                $this->twitterUrl         = $this->page['twitterUrl']         = empty($this->page->canonical_url) ? Request::url() : $this->page->canonical_url ;
                $this->twitterSiteName    = $this->page['twitterSiteName']    = $settings->twitter_sitename;
                $this->twitterCard        = $this->page['twitterCard']        = $settings->twitter_card;
            }
        }
    }
}