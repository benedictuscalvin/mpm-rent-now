<?php 
namespace Deptech\SeoExtension\Components;

class SeoPostPage extends SeoPage {
	public $hasBlog;

	public function componentDetails(){
		return [
			'name'        => 'SEO Post Page',
			'description' => 'Inject SEO Fields of CMS pages for News Page'
		];
	}

	public function defineProperties(){
		return [];
	}

	public function onRun() {
		$this->hasBlog = true;
	}
}