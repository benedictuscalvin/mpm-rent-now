<?php namespace Deptech\SeoExtension\Components;

use Validator;
use Flash;
use Mail;
use Lang;
use Redirect;
use BackendAuth;

class SeoStatic extends \Cms\Classes\ComponentBase
{
	public $page;
    public $title;
    public $seo_title;
    public $seo_description;
    public $seo_keywords;
    public $canonical_url;
    public $redirect_url;
    public $robot_index;
    public $robot_follow;
    public $hasNews;

    public $ogTitle;
    public $ogUrl;
    public $ogDescription;
    public $ogSiteName;
    public $ogFbAppId;
    public $ogLocale;
    public $ogImage;

    public function componentDetails()
    {
        return [
            'name'        => 'Seo Static Page',
            'description' => 'Seo for Static Page'
        ];
    }

    public function defineProperties()
    {
        return [
            'seo_title' => [
                'title'       => 'Seo title',
                'description' => 'Seo title',
                'type'        => 'string',
                'default'     => '',
            ],
            'seo_description' => [
                'title'       => 'Seo Description',
                'description' => 'Seo Description',
                'type'        => 'string',
                'default'     => '',
            ],
            'seo_keywords' => [
                'title'       => 'Seo Keywords',
                'description' => 'Seo Keywords',
                'type'        => 'string',
                'default'     => '',
            ],
   
        ];
    }

    public function onRun()
    {
        $this->page->meta_title = ($this->page->meta_title) ? $this->page->meta_title : $this->page->title;
		$this->seo_title        = $this->page["seo_title"]       = ($this->property('seo_title')) ? $this->property('seo_title') : $this->page->meta_title; 
        $this->title            = $this->page["title"]           = $this->seo_title; 
		$this->seo_description  = $this->page["seo_description"] = ($this->property('seo_description')) ? $this->property('seo_description') : $this->page->meta_description; 
		$this->seo_keywords     = $this->page["seo_keywords"]    = ($this->property('seo_keywords')) ? $this->property('seo_keywords') : $this->page->meta_keywords; 
    }
}