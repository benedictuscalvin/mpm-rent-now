<?php

namespace Deptech\SeoExtension\models;

use Model;

class Settings extends Model{

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'kidzrizal_seo_settings';

    public $settingsFields = 'fields.yaml';

    protected $cache = [];

    public $attachOne = [
        'og_image' => ['System\Models\File'],
        'favicon' => ['System\Models\File'],
        'appicon' => ['System\Models\File']
    ];

    public function getTwitterCardOptions(){
		return [
            "summary" => "summary",
            "summary_large_image" => "summary_large_image",
        ];
	}
} 