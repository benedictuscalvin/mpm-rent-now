<?php namespace Deptech\MpmRent\Helpers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
Use DB;
use Lang;
use Request;
use PhpParser\Node\Expr\Cast\Array_;

/**
* Helper khusus untuk Project MPMRent
* 
* 
* @package    OctoberCMS
* @subpackage Helpers
*/
Class ProjectHelpers {
	public static function getRegion()
	{
		$path = 'plugins/deptech/mpmrent/assets/region.json';
        $content = json_decode(file_get_contents($path), true);

        return $content;
	}
}