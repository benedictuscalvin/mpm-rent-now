<?php
namespace Deptech\MPMRent\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Deptech\MPMRent\Models\Rating as RatingModel;
use Config;

class Rating extends ReportWidgetBase
{
	public function widgetDetails() {
		return [
			'name' => 'Website Rating',
			'description' => 'Display website Rating'
		];
	}

	public function defineProperties()
    {
        return [
        ];
    }

	public function render()
    {
        try {
            $this->loadData();
        }
        catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    protected function loadData()
    {
        $rating = RatingModel::whereNull('deleted_at');

        $ratingCount = $rating->count();
        $ratingSum = $rating->sum('rating');
        $ratingResult = $ratingSum / $ratingCount;
        $this->vars['rating'] = $ratingResult;
    }
}