<?php namespace Deptech\MpmRent\Models;

use Model;
use BackendAuth;
use RainLab\Translate\Classes\Translator;

/**
 * Model
 */
class JobPositionModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_ref_job_position';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'job_departement_id' => ['Deptech\MPMRent\Models\JobDepartementModel', 'key' => 'ref_job_departement_id'],
        'user'       => ['Backend\Models\User', 'key' => 'user_id'],
    ];

    protected function beforeSave() { 
        $user = BackendAuth::getUser(); 
        $this->user_id = $user->id;
    }

    public function getRefJobDepartementIdOptions() {
        return JobDepartementModel::where('deleted_at', NULL)->lists('name', 'id');
    }

}
