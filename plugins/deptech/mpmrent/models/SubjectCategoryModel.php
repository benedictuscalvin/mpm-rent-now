<?php namespace Deptech\MpmRent\Models;

use Model;

/**
 * Model
 */
class SubjectCategoryModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_ref_subject_category';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    // public $belongsToMany = [
    //     'subject' => [
    //         'Deptech\MpmRent\Models\SubjectMailModel',
    //         'order'      => 'sort asc',
    //         'conditions' => 'deleted_at = null',
    //         'key'        => 'category_id',
    //         'otherKey' => 'category_id'
    //     ]
    // ];

    public $hasMany  = [
        'subject' => ['Deptech\MpmRent\Models\SubjectMailModel', 'table' => 'deptech_mpmrent_ref_subject', 'key' => 'category_id'],
    ];
}
