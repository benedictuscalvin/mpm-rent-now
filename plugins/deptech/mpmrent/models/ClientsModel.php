<?php namespace Deptech\MpmRent\Models;

use Model;

/**
 * Model
 */
class ClientsModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_clients';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
