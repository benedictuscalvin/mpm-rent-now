<?php namespace Deptech\MpmRent\Models;

use Model;

/**
 * Model
 */
class ContactUsModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_contact_us';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
