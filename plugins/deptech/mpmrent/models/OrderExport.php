<?php namespace Deptech\MpmRent\Models;

class OrderExport extends \Backend\Models\ExportModel
{
	public $table = 'deptech_mpmrent_order';

    public $belongsTo = [
        'service'            => ['Deptech\MpmRent\Models\RefOrderServices', 'key' => 'ref_service_id'],
        'region'             => ['Deptech\MpmRent\Models\RefOrderRegion', 'key'   => 'ref_region_id'],
        'service_needed_rel' => ['Deptech\MpmRent\Models\RefServiceNeeded', 'key' => 'service_needed'],
        'contact_hour'       => ['Deptech\MpmRent\Models\RefContactHour', 'key'   => 'eligible_to_contact'],
    ];

    protected $appends = [
        'service_name',
        'contact_hour_name',
        'service_needed_name',
        'region_name',
    ];
    
    public function exportData($columns, $sessionKey = null) {
        $query = self::make();
        /*
        you can filter anything in here
        */
        return $query->whereNull('deleted_at')->get()->toArray();
    }

    public function getServiceNameAttribute() {
        return $this->service->name;
    }
    
    public function getContactHourNameAttribute() {
        return $this->contact_hour->name;
    }

    public function getServiceNeededNameAttribute() {
        return $this->service_needed_rel->name;
    }

    public function getRegionNameAttribute() {
        return $this->region->name;
    }
}