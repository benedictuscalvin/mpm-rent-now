<?php namespace Deptech\MpmRent\Models;

use Model;

/**
 * Model
 */
class OrderForm extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_order';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'service'       => ['Deptech\MpmRent\Models\RefOrderServices', 'key' => 'ref_service_id'],
        'region'       => ['Deptech\MpmRent\Models\RefOrderRegion', 'key' => 'ref_region_id'],
    ];
}
