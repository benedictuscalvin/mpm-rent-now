<?php namespace Deptech\MpmRent\Models;

use Model;
use BackendAuth;
use RainLab\Translate\Classes\Translator;

/**
 * Model
 */
class CareerModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $translator;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_career';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'job_location' => ['Deptech\MPMRent\Models\JobLocationModel', 'key' => 'ref_location_id'],
        'position' => ['Deptech\MPMRent\Models\JobPositionModel', 'key' => 'ref_position_id'],
        // 'departement' => ['Deptech\MPMRent\Models\JobDepartementModel', 'key' => 'ref_job_departement_id'],
        'user'       => ['Backend\Models\User', 'key' => 'user_id'],
    ];

    protected function beforeSave() { 
        $user = BackendAuth::getUser(); 
        $this->user_id = $user->id;
    }

    public function getRefPositionIdOptions() {
        return JobPositionModel::where('deleted_at', NULL)->lists('name', 'id');
    }

    public function getRefLocationIdOptions() {
        return JobLocationModel::where('deleted_at', NULL)->lists('name', 'id');
    }
}
