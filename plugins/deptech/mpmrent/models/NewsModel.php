<?php namespace Deptech\MpmRent\Models;

use Model;
use BackendAuth;
use RainLab\Translate\Classes\Translator;

/**
 * Model
 */
class NewsModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title', 'description', 'seo_title', 'seo_description'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_news';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'user'       => ['Backend\Models\User', 'key' => 'user_id'],
    ];

    public $attachOne = [ 
        'images' => [
            'System\Models\File', 'order' => 'sort_order'
        ] 
    ];


    /**
     * @var array The accessors to append to the model's array form.
     */
    protected $appends = ['summary', 'has_summary'];

    protected function beforeSave() { 
        $user = BackendAuth::getUser(); 
        $this->user_id = $user->id;
    }

    public function scopeListFrontEnd($query, $options)
    {
        /*
         * Default options
         */
        extract(array_merge([
            'page'             => 1,
            'perPage'          => 30,
            'sort'             => 'event_date'
        ], $options));


        return $query->paginate($perPage, $page);
    }

    public function setUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->slug
        ];
        
        return $this->url = $controller->pageUrl($pageName, $params);
    }

    /**
     * Used by "has_summary", returns true if this post uses a summary (more tag).
     * @return boolean
     */
    public function getHasSummaryAttribute()
    {
        $more = '<!-- more -->';

        return (
            strpos($this->description, $more) !== false ||
            strlen(Html::strip($this->description)) > 100
        );
    }
}
