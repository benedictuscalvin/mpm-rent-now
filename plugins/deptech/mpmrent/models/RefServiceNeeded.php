<?php namespace Deptech\MpmRent\Models;

use Model;

/**
 * Model
 */
class RefServiceNeeded extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name'];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_ref_service_needed';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
