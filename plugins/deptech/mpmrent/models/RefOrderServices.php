<?php namespace Deptech\MpmRent\Models;

use Model;
use RainLab\Translate\Classes\Translator;

/**
 * Model
 */
class RefOrderServices extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name'];

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_ref_order_services';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
