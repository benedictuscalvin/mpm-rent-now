<?php namespace Deptech\MpmRent\Models;

use Deptech\MpmRent\Models\Store;

class ContactUsExport extends \Backend\Models\ExportModel
{
	public $table = 'deptech_mpmrent_contact_us';
    
    public function exportData($columns, $sessionKey = null) {
        $query = self::make();
        /*
        you can filter anything in here
        */
        return $query->whereNull('deleted_at')->get()->toArray();
    }
}