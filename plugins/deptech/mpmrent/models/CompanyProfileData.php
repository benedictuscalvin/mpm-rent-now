<?php namespace Deptech\MpmRent\Models;

use Model;

/**
 * Model
 */
class CompanyProfileData extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_company_profile_data';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
