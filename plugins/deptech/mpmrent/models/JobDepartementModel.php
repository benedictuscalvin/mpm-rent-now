<?php namespace Deptech\MpmRent\Models;

use Model;
use BackendAuth;
use RainLab\Translate\Classes\Translator;

/**
 * Model
 */
class JobDepartementModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_ref_job_departement';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'user'       => ['Backend\Models\User', 'key' => 'user_id'],
    ];

    protected function beforeSave() { 
        $user = BackendAuth::getUser(); 
        $this->user_id = $user->id;
    }
}
