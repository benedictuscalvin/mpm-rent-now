<?php namespace Deptech\MpmRent\Models;

use Model;
use Deptech\MPMRent\Models\SubjectCategoryModel;

/**
 * Model
 */
class SubjectMailModel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name','category_name'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_ref_subject';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'category' => ['Deptech\MPMRent\Models\SubjectCategoryModel', 'key' => 'category_id'],
    ];

    public function getSubjectCategoryIdOptions() {
        return SubjectCategoryModel::where('deleted_at', NULL)->orderby('sort')->lists('name', 'id');
    }
}
