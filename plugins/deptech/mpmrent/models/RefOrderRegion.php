<?php namespace Deptech\MpmRent\Models;

use Model;

/**
 * Model
 */
class RefOrderRegion extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'deptech_mpmrent_ref_order_region';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
