/** 
 * Share button
 */

$('.share-facebook').on('click', function(e) {
	e.preventDefault();
	var data_url = ($(this).attr('data-url')) ? $(this).attr('data-url') : '';
	share('facebook', data_url);
});

$('.share-whatsapp').on('click', function(e) {
	e.preventDefault();
	var data_url = ($(this).attr('data-url')) ? $(this).attr('data-url') : '';
	share('whatsapp', data_url);
});

$('.share-twitter').on('click', function(e) {
	e.preventDefault();
	var data_url = ($(this).attr('data-url')) ? $(this).attr('data-url') : '';
	share('twitter', data_url);
});

$('.share-linkedin').on('click', function(e) {
	e.preventDefault();
	var data_url = ($(this).attr('data-url')) ? $(this).attr('data-url') : '';
	share('linkedin', data_url);
});

$('.share-mail').on('click', function(e) {
	e.preventDefault();
	var data_url = ($(this).attr('data-url')) ? $(this).attr('data-url') : '';
	share('mail', data_url);
});

$('.share-copy').on('click', function(e) {
	e.preventDefault();
	var data_url = ($(this).attr('data-url')) ? $(this).attr('data-url') : '';
	share('copyurl', data_url);
});

function share(type, data_url = '') {
	var url = (data_url) ? data_url : window.location.href;
	
	if (type === 'facebook') {
		window.open("https://www.facebook.com/sharer/sharer.php?u="+url);
	} else if (type === 'whatsapp') {
		window.open("whatsapp://send?text="+customText + ' ' +url);
	} else if (type === 'twitter') {
		window.open("https://twitter.com/intent/tweet?text="+customText + ' ' +url);
	} else if (type === 'instagram') {

	} else if (type === 'linkedin') {
		window.open("https://www.linkedin.com/sharing/share-offsite/?url="+url);
	} else if (type === 'mail') {
		window.open("mailto:?subject="+ customSubject +"&body="+customText + ' ' +url);
	} else if (type === 'copyurl') {
		copyUrl();
	}
}

function copyUrl() {
	if (!window.getSelection) {
		alert('Please copy the URL from the location bar.');
		return;
	}
	const dummy = document.createElement('p');
	dummy.textContent = window.location.href;
	document.body.appendChild(dummy);

	const range = document.createRange();
	range.setStartBefore(dummy);
	range.setEndAfter(dummy);

	const selection = window.getSelection();
	// First clear, in case the user already selected some other text
	selection.removeAllRanges();
	selection.addRange(range);

	document.execCommand('copy');
	document.body.removeChild(dummy);

  	$.notify({
		// options
		message: 'Link copied!' 
	},{
		// settings
		type: 'success'
	});

  $('#shareModal').modal('toggle');
}

serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}