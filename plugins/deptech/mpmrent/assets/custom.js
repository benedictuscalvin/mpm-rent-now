function stripHtml(html){
    // Create a new div element
    var temporalDivElement = document.createElement("div");
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
}

String.prototype.trimToLength = function(m) {
  return (this.length > m) 
    ? jQuery.trim(this).substring(0, m).split(" ").slice(0, -1).join(" ") + "..."
    : this;
};

$(document).on('keyup', '#MLRichEditor-formDescription-mlControl-description', function() {
	if ($('#MLRichEditor-formDescription-mlControl-description button.ml-btn').html() === 'en') {
		var value = stripHtml($('#MLRichEditor-formDescription-textarea-description').val().trim()).trimToLength(155);
		$('#Form-field-NewsModel-seo_description').val(value);
	}
});

function setTranslation(element, text, from, to) {
  $.ajax({
      type:"GET",
      headers: {
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'http://localhost',
        'Access-Control-Allow-Methods':'GET, POST',
        'Access-Control-Allow-Headers':'Origin, Content-Type, Accept',
      },
      url: "http://t.song.work/api?text="+text+"&from="+from+"&to="+to, 
      success: function(data) {
        console.log(data)
              // $(element).val(JSON.stringify(data));
          }, 
      error: function(jqXHR, textStatus, errorThrown) {
          },
    dataType: "json",
    crossDomain: true,
  })
}

$(document).on('keyup', '#Form-field-NewsModel-title-placeholderField', function() {
  if ($('#MLText-formTitle-title button').html() === 'en') {
    $('[name="RLTranslate[en][seo_title]"]').val($(this).val());
    $('[name="NewsModel[seo_title]"]').val($(this).val());
    setTranslation('[name="RLTranslate[id][seo_title]"]', $(this).val(), 'en', 'id');
    // setTranslation('[name="RLTranslate[jp][seo_title]"]', $(this).val(), 'en', 'ja');
	}
});

$(document).on('keyup', '#MLRichEditor-formDescription-mlControl-description', function() {
	if ($('#MLRichEditor-formDescription-mlControl-description button.ml-btn').html() === 'en') {
		var value = stripHtml($('#MLRichEditor-formDescription-textarea-description').val().trim().replace(/(<([^>]+)>)/ig,"")).trimToLength(155);
		$('#Form-field-Guides-seo_description').val(value);
    $('[name="NewsModel[seo_description]"]').val(value);
    $('[name="RLTranslate[en][seo_description]"]').val(value);
	}
});
