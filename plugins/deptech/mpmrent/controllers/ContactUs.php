<?php namespace Deptech\MpmRent\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class ContactUs extends Controller
{
    public $implement = [        
    	'Backend\Behaviors\ListController',
    	\Backend\Behaviors\ImportExportController::class,
    ];
    
    public $listConfig = 'config_list.yaml';
    public $requiredPermissions = ['deptech.mpmrent.contact_us'];
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Deptech.MpmRent', 'main-plugin', 'mpm-contact-us');
    }
}
