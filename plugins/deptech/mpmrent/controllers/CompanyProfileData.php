<?php namespace Deptech\MpmRent\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class CompanyProfileData extends Controller
{
    public $implement = [        
    	'Backend\Behaviors\ListController',
    	\Backend\Behaviors\ImportExportController::class,
    ];
    
	public $requiredPermissions = ['deptech.mpmrent.company_profile_data'];
	public $importExportConfig = 'config_import_export.yaml';    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Deptech.MpmRent', 'main-plugin', 'side-menu-compro');
    }
}
