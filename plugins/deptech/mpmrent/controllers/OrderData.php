<?php namespace Deptech\MpmRent\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class OrderData extends Controller
{
    public $implement = [        
    	'Backend\Behaviors\ListController',
    	\Backend\Behaviors\ImportExportController::class,    
    ];
    
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['deptech.mpmrent.order_data'];
	public $importExportConfig = 'config_import_export.yaml'; 

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Deptech.MpmRent', 'main-plugin', 'side-menu-order-data');
    }
}
