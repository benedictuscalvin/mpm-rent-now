<?php namespace Deptech\MpmRent\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Testimonial extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $requiredPermissions = ['deptech.mpmrent.testimonial'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Deptech.MpmRent', 'main-plugin', 'mpm-testimonial');
    }
}
