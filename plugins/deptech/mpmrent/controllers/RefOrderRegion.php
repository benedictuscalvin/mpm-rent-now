<?php namespace Deptech\MpmRent\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

use Deptech\MpmRent\Models\RefOrderRegion as Model;
use Deptech\MpmRent\Helpers\ProjectHelpers;

class RefOrderRegion extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Deptech.MpmRent', 'main-menu-master', 'side-menu-order-region');
    }

    public function onLoadResetRegion()
    {
        return $this->makePartial('reset_region');
    }

    public function onResetRegion()
    {
        Model::query()->truncate();
        $region = ProjectHelpers::getRegion();

        foreach ($region as $key => $value) {
            $refRegion = new Model;
            $refRegion->name = $value;
            $refRegion->code = strtolower(str_replace(' ', '-', $value));
            $refRegion->is_available = 0;
            $refRegion->save();
        }

        return $this->listRefresh();
    }
}
