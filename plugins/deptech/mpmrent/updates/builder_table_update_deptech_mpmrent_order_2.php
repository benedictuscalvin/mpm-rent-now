<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentOrder2 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->string('registration_number')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->dropColumn('registration_number');
        });
    }
}
