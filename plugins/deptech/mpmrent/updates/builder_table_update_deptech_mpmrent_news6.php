<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentNews6 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_news', function($table)
        {
            $table->string('seo_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_news', function($table)
        {
            $table->dropColumn('seo_title');
        });
    }
}