<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDeptechMpmrentProfiles extends Migration
{
    public function up()
    {
        Schema::create('deptech_mpmrent_profiles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('position');
            $table->text('description');
            $table->string('images');
            $table->integer('user_id')->unsigned();
            $table->smallInteger('is_publish')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('deptech_mpmrent_profiles');
    }
}
