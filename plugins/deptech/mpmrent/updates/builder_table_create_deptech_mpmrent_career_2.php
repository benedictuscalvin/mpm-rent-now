<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDeptechMpmrentCareer2 extends Migration
{
    public function up()
    {
        Schema::create('deptech_mpmrent_career', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('ref_position_id')->unsigned();
            $table->integer('ref_location_id')->unsigned();
            $table->integer('ref_job_time_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->date('expired_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->smallInteger('is_active')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('deptech_mpmrent_career');
    }
}
