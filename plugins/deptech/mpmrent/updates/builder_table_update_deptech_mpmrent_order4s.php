<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentOrder4s extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->integer('ref_service_id')->nullable();
            $table->dropColumn('order');
            $table->dropColumn('company_size');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->dropColumn('ref_service_id');
            $table->string('order', 191)->nullable();
            $table->string('company_size', 191);
        });
    }
}