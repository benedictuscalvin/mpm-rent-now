<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentCareer extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_career', function($table)
        {
            $table->integer('available_position');
            $table->text('description')->default(null)->change();
            $table->date('expired_at')->default(null)->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_career', function($table)
        {
            $table->dropColumn('available_position');
            $table->text('description')->default('NULL')->change();
            $table->date('expired_at')->default('NULL')->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
            $table->timestamp('deleted_at')->nullable()->default('NULL');
        });
    }
}
