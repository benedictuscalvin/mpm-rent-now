<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentTestimonial7 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_testimonial', function($table)
        {
            $table->string('company');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_testimonial', function($table)
        {
            $table->dropColumn('company');
        });
    }
}
