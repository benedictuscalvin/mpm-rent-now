<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentOrder3 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->renameColumn('registration_number', 'order_number');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->renameColumn('order_number', 'registration_number');
        });
    }
}
