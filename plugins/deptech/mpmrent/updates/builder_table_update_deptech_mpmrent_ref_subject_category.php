<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentRefSubjectCategory extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_ref_subject_category', function($table)
        {
            $table->smallInteger('sort')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_ref_subject_category', function($table)
        {
            $table->dropColumn('sort');
        });
    }
}
