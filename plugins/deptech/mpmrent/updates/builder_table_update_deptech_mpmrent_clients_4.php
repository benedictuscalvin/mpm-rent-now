<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentClients4 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_clients', function($table)
        {
            $table->integer('sort')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_clients', function($table)
        {
            $table->dropColumn('sort');
        });
    }
}
