<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDeptechMpmrentOrder extends Migration
{
    public function up()
    {
        Schema::create('deptech_mpmrent_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('order')->nullable();
            $table->string('company_name');
            $table->string('company_size');
            $table->string('position')->nullable();
            $table->string('region')->nullable();
            $table->integer('service_needed')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('deptech_mpmrent_order');
    }
}
