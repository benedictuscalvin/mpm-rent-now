<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentOrder2 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->string('service_needed', 10)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->integer('service_needed')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}