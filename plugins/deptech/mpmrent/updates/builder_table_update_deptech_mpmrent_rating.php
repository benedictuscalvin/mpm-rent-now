<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentRating extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_rating', function($table)
        {
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_rating', function($table)
        {
            $table->dropColumn('id');
        });
    }
}
