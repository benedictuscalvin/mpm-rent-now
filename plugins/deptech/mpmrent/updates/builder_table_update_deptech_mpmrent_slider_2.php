<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentSlider2 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_slider', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('description', 191)->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_slider', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->string('description', 191)->default('NULL')->change();
        });
    }
}
