<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDeptechMpmrentRefJobDepartement extends Migration
{
    public function up()
    {
        Schema::create('deptech_mpmrent_ref_job_departement', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->smallInteger('is_delete')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('deptech_mpmrent_ref_job_departement');
    }
}
