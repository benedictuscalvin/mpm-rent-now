<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDeptechMpmrentRating extends Migration
{
    public function up()
    {
        Schema::create('deptech_mpmrent_rating', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('score');
            $table->integer('total_user');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('deptech_mpmrent_rating');
    }
}
