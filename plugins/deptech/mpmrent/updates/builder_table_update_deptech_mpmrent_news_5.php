<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentNews5 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_news', function($table)
        {
            $table->string('seo_description')->nullable();
            $table->string('seo_keywords')->nullable();
            $table->string('canonical_url')->nullable();
            $table->string('redirect_url')->nullable();
            $table->string('robot_index')->nullable();
            $table->string('robot_follow')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_news', function($table)
        {
            $table->dropColumn('seo_description');
            $table->dropColumn('seo_keywords');
            $table->dropColumn('canonical_url');
            $table->dropColumn('redirect_url');
            $table->dropColumn('robot_index');
            $table->dropColumn('robot_follow');
        });
    }
}
