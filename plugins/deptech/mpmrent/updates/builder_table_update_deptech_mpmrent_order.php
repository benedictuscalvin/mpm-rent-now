<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentOrder extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->string('eligible_to_contact')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->dropColumn('eligible_to_contact');
        });
    }
}
