<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentOrder4 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->string('company_phone')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_order', function($table)
        {
            $table->dropColumn('company_phone');
        });
    }
}