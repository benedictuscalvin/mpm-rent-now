<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentSlider3 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_slider', function($table)
        {
            $table->integer('sort')->default(0)->change();
            $table->string('description', 191)->default('null')->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_slider', function($table)
        {
            $table->integer('sort')->default(null)->change();
            $table->string('description', 191)->default('\'null\'')->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
            $table->timestamp('deleted_at')->nullable()->default('NULL');
        });
    }
}
