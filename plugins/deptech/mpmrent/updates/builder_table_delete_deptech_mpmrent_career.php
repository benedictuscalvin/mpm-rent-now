<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteDeptechMpmrentCareer extends Migration
{
    public function up()
    {
        Schema::dropIfExists('deptech_mpmrent_career');
    }
    
    public function down()
    {
        Schema::create('deptech_mpmrent_career', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 191);
            $table->text('description')->nullable()->default('NULL');
            $table->integer('ref_position_id')->unsigned();
            $table->integer('ref_location_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->date('expired_at')->nullable()->default('NULL');
            $table->timestamp('deleted_at')->nullable()->default('NULL');
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
            $table->smallInteger('is_active')->default(0);
        });
    }
}
