<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentRefSubject2 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_ref_subject', function($table)
        {
            $table->integer('category_id')->nullable()->unsigned();
            $table->dropColumn('category_name');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_ref_subject', function($table)
        {
            $table->dropColumn('category_id');
            $table->string('category_name', 191)->nullable();
        });
    }
}
