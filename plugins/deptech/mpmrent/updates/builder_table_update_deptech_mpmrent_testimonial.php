<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentTestimonial extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_testimonial', function($table)
        {
            $table->integer('user_id')->unsigned();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_testimonial', function($table)
        {
            $table->dropColumn('user_id');
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
            $table->timestamp('deleted_at')->nullable()->default('NULL');
        });
    }
}
