<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentRefSubject extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_ref_subject', function($table)
        {
            $table->string('category_name')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_ref_subject', function($table)
        {
            $table->dropColumn('category_name');
        });
    }
}
