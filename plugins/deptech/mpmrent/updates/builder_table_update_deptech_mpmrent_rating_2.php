<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDeptechMpmrentRating2 extends Migration
{
    public function up()
    {
        Schema::table('deptech_mpmrent_rating', function($table)
        {
            $table->integer('rating');
            $table->string('message')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->dropColumn('score');
            $table->dropColumn('total_user');
        });
    }
    
    public function down()
    {
        Schema::table('deptech_mpmrent_rating', function($table)
        {
            $table->dropColumn('rating');
            $table->dropColumn('message');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->integer('score');
            $table->integer('total_user');
        });
    }
}