<?php namespace Deptech\MpmRent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteDeptechMpmrentRefJobPosition2 extends Migration
{
    public function up()
    {
        Schema::dropIfExists('deptech_mpmrent_ref_job_position');
    }
    
    public function down()
    {
        Schema::create('deptech_mpmrent_ref_job_position', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->integer('ref_job_departement_id')->unsigned();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
            $table->timestamp('deleted_at')->nullable()->default('NULL');
        });
    }
}
