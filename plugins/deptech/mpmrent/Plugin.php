<?php namespace Deptech\MpmRent;

use System\Classes\PluginBase;
use Deptech\MpmRent\Models\Settings as SettingsModel;
use Event;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
            'Deptech\MPMRent\Components\Map'            => 'map',
            'Deptech\MPMRent\Components\Slider'         => 'slider',
            'Deptech\MPMRent\Components\Testimonial'    => 'testimonial',
            'Deptech\MPMRent\Components\Partner'        => 'partner',
            'Deptech\MPMRent\Components\News'           => 'news',
            'Deptech\MPMRent\Components\News_detail'    => 'news_detail',
            'Deptech\MPMRent\Components\Contact_us'     => 'contact_us',
            'Deptech\MPMRent\Components\Profile'        => 'profile',
            'Deptech\MPMRent\Components\Clients'        => 'client',
            'Deptech\MPMRent\Components\CompanyProfile' => 'company_profile',
            'Deptech\MPMRent\Components\OrderForm'      => 'order_form',
            'Deptech\MPMRent\Components\Rating'         => 'rating',
        ];
    }

    public function boot()
    {
        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
           $controller->addJs('/plugins/deptech/mpmrent/assets/custom.js');
        });
    }

    public function registerPermissions()
    {
        return [
            'deptech.mpmrent.career' => [
                'tab'   => 'Career',
                'label' => 'Career Access'
            ],
            'deptech.mpmrent.news' => [
                'tab'   => 'News',
                'label' => 'News Access'
            ],
            'deptech.mpmrent.mpm_plugin' => [
                'tab'   => 'MPM Plugin',
                'label' => 'MPM Plugin Access'
            ],
            'deptech.mpmrent.office_location' => [
                'tab'   => 'MPM Plugin',
                'label' => 'Office Location Access'
            ],
            'deptech.mpmrent.slider' => [
                'tab'   => 'MPM Plugin',
                'label' => 'Slider Access'
            ],
            'deptech.mpmrent.testimonial' => [
                'tab'   => 'MPM Plugin',
                'label' => 'Testimonial Access'
            ],
            'deptech.mpmrent.clients' => [
                'tab'   => 'MPM Plugin',
                'label' => 'Clients Access'
            ],
            'deptech.mpmrent.partner' => [
                'tab'   => 'MPM Plugin',
                'label' => 'Partner Access'
            ],
            'deptech.mpmrent.contact_us' => [
                'tab'   => 'MPM Plugin',
                'label' => 'Contact us Access'
            ],
            'deptech.mpmrent.profiles' => [
                'tab'   => 'MPM Plugin',
                'label' => 'Profiles Access'
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label' => 'Site Config',
                'description' => 'Config for SKOR Website',
                'category' => 'system::lang.system.categories.cms',
                'icon' => 'icon-cog',
                'class' => 'Deptech\MpmRent\Models\Settings',
                'order' => 500,
                'keywords' => 'config',
                // 'permissions' => ['deptech.mpmrent.config']
            ]
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'dateftimestamp' => function($timestamp = '', $format = 'd F Y') {
                    $timestamp = strtotime($timestamp);
                    return Carbon::createFromTimestamp($timestamp)->format($format);
                },
                'getSettings' => function($code = '', $value = '') {
                    if ($value)
                    {
                        return SettingsModel::get($code, $value);
                    }
                    else 
                    {
                        return SettingsModel::get($code);
                    }
                },
            ],
       ];
    }

    public function registerReportWidgets()
    {
        return [
            'Deptech\MpmRent\ReportWidgets\Rating' => [
                'label'       => 'Rating overview',
                'context'     => 'dashboard',
            ],
        ];
    }
}
