<?php return [
    'plugin' => [
        'name' => 'MPM Rent',
        'description' => 'Plugin untuk membantu project MPM Rent'
    ],
    'app' => [
    	'order_form' => [
			'name_required'                => "Harap mengisi kolom 'Nama Lengkap'",
			'email_required'               => "Harap mengisi kolom 'Email'",
			'phone_required'               => "Harap mengisi kolom 'No. HP'",
			'phone_min'                    => "Kolom 'No. HP' minimal 10 angka",
			'phone_max'                    => "Kolom 'No. HP' maksimal 14 angka",
			'phone_phone_number'           => "No. HP harus diawali dengan +62",
			'company_name_required'        => "Harap mengisi kolom 'Perusahaan'",
			'company_phone_required'       => "Harap mengisi kolom 'No. Telp Kantor'",
			'company_phone_min'            => "Kolom 'No. Telp Kantor' minimal 10 angka",
			'company_phone_max'            => "Kolom 'No. Telp Kantor' maksimal 14 angka",
			'ref_service_id_required'      => "Harap mengisi kolom 'Pilihan Layanan'",
			'position_required'            => "Harap mengisi kolom 'Jabatan'",
			'region_required'              => "Harap mengisi kolom 'Wilayah'",
			'eligible_to_contact_required' => "Harap mengisi kolom 'Bersedia Di Hubungi'",
			'service_needed_required'      => "Harap mengisi kolom 'Kebutuhan'",
    	]
    ]
];