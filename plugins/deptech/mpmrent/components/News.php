<?php namespace Deptech\MPMRent\Components;

use Lang;
use Redirect;
use BackendAuth;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use October\Rain\Database\Model;
use October\Rain\Database\Collection;
use Deptech\MPMRent\Models\NewsModel;
use RainLab\Translate\Classes\Translator;

class News extends \Cms\Classes\ComponentBase
{
    /**
     * A collection of posts to display
     *
     * @var Collection
     */
    public $posts;

    /**
     * Parameter to use for the page number
     *
     * @var string
     */
    public $pageParam;

    /**
     * Message to display when there are no messages
     *
     * @var string
     */
    public $noPostsMessage;

    /**
     * Reference to the page name for linking to posts
     *
     * @var string
     */
    public $postPage;
    /**
     * If the post list should be ordered by another attribute
     *
     * @var string
     */
    public $sortOrder;

    public function componentDetails()
    {
        return [
            'name'        => 'Post News',
            'description' => 'A Post'
        ];
    }

    public function defineProperties()
    {
        return [
            'pageNumber' => [
                'title'       => 'Page Number',
                'description' => 'Page Number',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'postsPerPage' => [
                'title'             => 'Perpage',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Invalid format of the posts per page value',
                'default'           => '4',
            ],
            'sortOrder' => [
                'title'       => 'Post order',
                'description' => 'Attribute on which the posts should be ordered',
                'type'        => 'dropdown',
                'default'     => 'event_date desc',
            ],
            'postPage' => [
                'title'       => 'Post page',
                'description' => 'Name of the blog post page file for the "Learn more" links. This property is used by the default component partial.',
                'type'        => 'dropdown',
                'default'     => 'news',
                'group'       => 'Links',
            ],
            'exceptPost' => [
                'title'             => 'Except post',
                'description'       => 'Enter ID/URL or variable with post ID/URL you want to exclude. You may use a comma-separated list to specify multiple posts.',
                'type'              => 'string',
                'validationPattern' => '^[a-z0-9\-_,\s]+$',
                'validationMessage' => 'Post exceptions must be a single slug or ID, or a comma-separated list of slugs and IDs',
                'default'           => '',
                'group'             => 'Exceptions',
            ]
        ];
    }
    
    public function getPostPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->prepareVars();
        if ($this->property('pageNumber') == 'news.mdb') {
        }
        
        $translator = Translator::instance();
        $activeLocale = $translator->getLocale();
        if ($activeLocale == 'jp') {
            return Redirect::to('/jcbu');
        }

        $this->posts = $this->page['posts'] = $this->listPosts();

        /*
         * If the page number is not valid, redirect
         */
        if ($pageNumberParam = $this->paramName('pageNumber')) {
            $currentPage = $this->property('pageNumber');

            if ($currentPage > ($lastPage = $this->posts->lastPage()) && $currentPage > 1) {
                return Redirect::to($this->currentPageUrl([$pageNumberParam => $lastPage]));
            }
        }
        $this->addJs('/plugins/deptech/mpmrent/assets/sharing-button.js');
    }

    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->noPostsMessage = $this->page['noPostsMessage'] = $this->property('noPostsMessage');

        /*
         * Page links
         */
        $this->postPage = $this->page['postPage'] = $this->property('postPage');
    }

    protected function listPosts()
    {
        /*
         * List all the news posts
         */

        trace_sql();
        $posts = NewsModel::where([
            ['deleted_at', NULL],
            ['is_active', 1]
        ])->orderBy('event_date', 'desc')->listFrontEnd([
            'page'       => $this->property('pageNumber'),
            'perPage'    => $this->property('postsPerPage'),
            'sort'       => $this->property('sortOrder'),
            'search'           => trim(input('search')),
            'exceptPost'       => preg_split('/,\s*/', $this->property('exceptPost'), -1, PREG_SPLIT_NO_EMPTY),
        ]);

        /*
         * Add a "url" helper attribute for linking to each post
         */
        $posts->each(function($post) {
            $post->setUrl($this->postPage, $this->controller);
        });
        
        return $posts;
    }
}