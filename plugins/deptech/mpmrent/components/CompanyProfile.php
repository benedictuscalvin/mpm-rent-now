<?php namespace Deptech\MPMRent\Components;

use Cms\Classes\ComponentBase;
use RainLab\Translate\Classes\Translator;
use Deptech\MPMRent\Models\CompanyProfileData;
// use Deptech\MPMRent\Models\SubjectMailModel;
// use Deptech\MPMRent\Models\SubjectCategoryModel;
use Carbon\Carbon;
use Deptech\MPMRent\Models\Settings;
use Crypt;

use DB;
use Mail;
use Input;
use Flash;
use Lang;
use Redirect;
use Config;
use Validator;

class CompanyProfile extends ComponentBase
{
	protected $translator;

    public function componentDetails()
    {
        return [
            'name'        => 'Company Profile Form Component',
            'description' => 'Company Profile Form Component'
        ];
    }

    public function defineProperties()
    {
        return [
            'mail_to' => [
                'title' => 'Send email to',
                'description' => 'Send email to',
                'default' => 'marketing@mpm-rent.com',
                'type' => 'string',
                'required' => true,
            ],
            'mail_fullname' => [
                'title' => 'Sender fullname',
                'description' => 'Sender fullname',
                'default' => 'Marketing',
                'type' => 'string',
                'required' => true,
            ],
        ];
    }

    public function onRun() {

    }

    public function onSubmit() {

        try {

            // Begin a transaction
            DB::beginTransaction();

            // catch post value
            $post = Input::all();

            $messages = [
                'name.required'             => Lang::get('deptech.mpmrent::lang.app.company_profile.name_required'),
                'email.required'            => Lang::get('deptech.mpmrent::lang.app.company_profile.email_required'),
                'phone_number.required'     => Lang::get('deptech.mpmrent::lang.app.company_profile.phone_required'),
                'phone_number.min'          => Lang::get('deptech.mpmrent::lang.app.company_profile.phone_min'),
                'phone_number.max'          => Lang::get('deptech.mpmrent::lang.app.company_profile.phone_max'),
                'phone_number.phone_number' => Lang::get('deptech.mpmrent::lang.app.company_profile.phone_phone_number'),
                'company_name.required'     => Lang::get('deptech.mpmrent::lang.app.company_profile.company_name_required'),
            ];

            Validator::extend('phone_number', function($attribute, $value, $parameters) {
                return (substr($value, 0, 3) == '+62' || substr($value, 0, 1) == '0');
            });

            $validator = Validator::make($post, [
                'name'         => 'required|string',
                'email'        => 'required|email',
                'phone_number' => 'required|max:14|min:10|phone_number',
                'company_name' => 'required',
            ], $messages);

            if ($validator->fails()) {
                $message = '';
                foreach($validator->errors()->all() as $key => $value) {
                    $message .= '<p>'.$value.'</p>';
                }
                Flash::error($message);
                return;
            }

            $company_profile = new CompanyProfileData;
            
            $company_profile->name         = $post['name'];
            $company_profile->email        = $post['email'];
            $company_profile->phone_number = $post['phone_number'];
            $company_profile->company_name = $post['company_name'];
            $company_profile->created_at = Carbon::now('Asia/Jakarta');
            
            $company_profile->save();
            
            $id = $company_profile->id;
            
            
            DB::commit();
            $this->sendEmailContact($post);
            $this->sendEmailAdmin($post);
            $slug = Crypt::encrypt($id);
            
            // Flash::success('Email successfully sent.');
            
            $this->translator = Translator::instance();
            return Redirect::to($this->translator->getLocale().'/company-profile/thanks/'. $slug);

        } catch (\Exception $e) {
            // An error occured; cancel the transaction...
            DB::rollback();

            // and throw the error again.
            throw $e;
            // Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }

    }

    protected function sendEmailContact(Array $sender) {
        try {
            $mail_to = ($this->property('mail_to')) ? $this->property('mail_to') : 'contact@mpm-rent.com';
            $vars = [
                'fullname'           => $sender['name'],
                'email'              => $sender['email'],
                'phone_number'       => $sender['phone_number'],
                'company_name'       => $sender['company_name'],
                'whatsapp'           => Settings::get('whatsapp'),
                // 'subject'         => $sender['subject'],
                // 'contact_message' => $sender['message'],
                'mail_to'            => $sender['email'],
                'mail_fullname'      => $this->property('mail_fullname'),
                'attachment'         => (Settings::get('compro_file')) ? public_path() . '/' . Config::get('cms.storage.media.path'). Settings::get('compro_file') : null
            ];
            Mail::send('mpmrent::mail.companyprofile', $vars, function($message) use ($vars) {
            	// print_r();exit;
                $message->to($vars['mail_to'], $vars['mail_fullname']);
                $message->subject("[MPMRent] $vars[fullname], Terima Kasih atas Permintaan Anda!");
                if ($vars['attachment'] ) {
                    $message->attach($vars['attachment']);
                }
                $message->data = $vars;
            });
            Flash::success('Mail has been sent !');
        } catch (\Exception $e) {
            // and throw the error again.
            // Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }
    }

    protected function sendEmailAdmin(Array $sender) {
        try {
            $mail_to = ($this->property('mail_to')) ? $this->property('mail_to') : 'contact@mpm-rent.com';
            $vars = [
                'fullname'           => $sender['name'],
                'email'              => $sender['email'],
                'phone_number'       => $sender['phone_number'],
                'company_name'       => $sender['company_name'],
                // 'subject'         => $sender['subject'],
                // 'contact_message' => $sender['message'],
                'mail_to'            => $mail_to,
                'mail_fullname'      => $this->property('mail_fullname'),
                'attachment'         => (Settings::get('compro_file')) ? url(Config::get('cms.storage.media.path'). Settings::get('compro_file')) : null
            ];
            Mail::send('mpmrent::companyprofile', $vars, function($message) use ($vars) {
            	// print_r();exit;
                $message->to($vars['mail_to'], $vars['mail_fullname']);
                // $message->subject("[MPMRent] $vars[fullname], Terima Kasih atas Permintaan Anda!");
                $message->data = $vars;
            });
            Flash::success('Mail has been sent !');
        } catch (\Exception $e) {
            // and throw the error again.
            // Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }
    }
}