<?php namespace Deptech\MPMRent\Components;

use Cms\Classes\ComponentBase;
use RainLab\Translate\Classes\Translator;
use Deptech\MPMRent\Models\OrderForm as Order;
use Carbon\Carbon;
use Deptech\MPMRent\Models\Settings;
use Deptech\MPMRent\Models\RefOrderServices;
use Deptech\MPMRent\Models\RefOrderRegion;
use Deptech\MPMRent\Models\RefContactHour;
use Deptech\MPMRent\Models\RefServiceNeeded;
use Crypt;

use DB;
use Mail;
use Input;
use Flash;
use Redirect;
use Config;
use Validator;
use Lang;

class OrderForm extends ComponentBase
{
	protected $translator;

    public function componentDetails()
    {
        return [
            'name'        => 'Order Form Component',
            'description' => 'Order Form Component'
        ];
    }

    public function defineProperties()
    {
        return [
            'mail_to' => [
                'title' => 'Send email to',
                'description' => 'Send email to',
                'default' => 'marketing@mpm-rent.com',
                'type' => 'string',
                'required' => true,
            ],
            'mail_fullname' => [
                'title' => 'Sender fullname',
                'description' => 'Sender fullname',
                'default' => 'Marketing',
                'type' => 'string',
                'required' => true,
            ],
        ];
    }

    public function onRun() {
        $this->page['services']       = RefOrderServices::whereNull('deleted_at')->orderBy('name')->get();
        $this->page['regions']        = RefOrderRegion::whereNull('deleted_at')->orderByDesc('is_available')->orderBy('id')->get();
        $this->page['contact_hour']   = RefContactHour::whereNull('deleted_at')->orderBy('name')->get();
        $this->page['service_needed'] = RefServiceNeeded::orderBy('name')->get();
    }

    public function onSubmit() {

        try {
            // Begin a transaction
            DB::beginTransaction();

            // catch post value
            $post = Input::all();

            $messages = [
                'name.required'                => Lang::get('deptech.mpmrent::lang.app.order_form.name_required'),
                'email.required'               => Lang::get('deptech.mpmrent::lang.app.order_form.email_required'),
                'phone.required'               => Lang::get('deptech.mpmrent::lang.app.order_form.phone_required'),
                'phone.min'                    => Lang::get('deptech.mpmrent::lang.app.order_form.phone_min'),
                'phone.max'                    => Lang::get('deptech.mpmrent::lang.app.order_form.phone_max'),
                'phone.phone_number'           => Lang::get('deptech.mpmrent::lang.app.order_form.phone_phone_number'),
                'company_name.required'        => Lang::get('deptech.mpmrent::lang.app.order_form.company_name_required'),
                'company_phone.required'       => Lang::get('deptech.mpmrent::lang.app.order_form.company_phone_required'),
                'company_phone.min'            => Lang::get('deptech.mpmrent::lang.app.order_form.company_phone_min'),
                'company_phone.max'            => Lang::get('deptech.mpmrent::lang.app.order_form.company_phone_max'),
                'company_phone.phone_number'   => Lang::get('deptech.mpmrent::lang.app.order_form.phone_phone_number'),
                'ref_service_id.required'      => Lang::get('deptech.mpmrent::lang.app.order_form.ref_service_id_required'),
                'position.required'            => Lang::get('deptech.mpmrent::lang.app.order_form.position_required'),
                'region.required'              => Lang::get('deptech.mpmrent::lang.app.order_form.region_required'),
                'eligible_to_contact.required' => Lang::get('deptech.mpmrent::lang.app.order_form.eligible_to_contact_required'),
                'service_needed.required'      => Lang::get('deptech.mpmrent::lang.app.order_form.service_needed_required'),
            ];

            Validator::extend('phone_number', function($attribute, $value, $parameters) {
                return (substr($value, 0, 3) == '+62' || substr($value, 0, 1) == '0');
            });

            $validator = Validator::make($post, [
                'name'                => 'required|string',
                'email'               => 'required|email',
                'phone'               => 'required|max:14|min:10|phone_number',
                'company_name'        => 'required',
                'company_phone'       => 'required|max:14|min:10|phone_number',
                'ref_service_id'      => 'required',
                'position'            => 'required',
                'region'              => 'required',
                'eligible_to_contact' => 'required',
                'service_needed'      => 'required',
            ], $messages);

            if ($validator->fails()) {
                $message = '';
                foreach($validator->errors()->all() as $key => $value) {
                    $message .= '<p>'.$value.'</p>';
                }
                Flash::error($message);
                return;
            }
            $id_last = Order::whereNull('deleted_at')->orderByDesc('id')->get()->first();
            $id_last = (isset($id_last->id)) ? $id_last->id : 0;
            $n = '0';
            $order_number = 'ORD-'.str_pad($n + $id_last, 6, 0, STR_PAD_LEFT);

            $order = new Order;
            
            $order->order_number        = $order_number;
            $order->name                = $post['name'];
            $order->email               = $post['email'];
            $order->phone               = $post['phone'];
            $order->company_name        = $post['company_name'];
            $order->company_phone       = $post['company_phone'];
            $order->position            = $post['position'];
            $order->region              = $post['region'];
            $order->eligible_to_contact = $post['eligible_to_contact'];
            $order->service_needed      = $post['service_needed'];
            $order->ref_service_id      = $post['ref_service_id'];
            $order->created_at          = $post['created_at'] = Carbon::now('Asia/Jakarta');

            $order->save();

            $id = $order->id;


            DB::commit();
            $slug = Crypt::encrypt($id);
            $post['order_number'] = $order_number;
            $this->sendEmail($post);

            $this->translator = Translator::instance();
            return Redirect::to($this->translator->getLocale().'/order/thanks/'. $slug);

        } catch (\Exception $e) {
            // An error occured; cancel the transaction...
            DB::rollback();

            // and throw the error again.
            throw $e;
            // Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }

    }

    protected function sendEmail(Array $sender) {
        try {
            $mail_to = ($this->property('mail_to')) ? $this->property('mail_to') : 'contact@mpm-rent.com';

            $vars = [
                'order_number'        => $sender['order_number'],
                'name'                => $sender['name'],
                'email'               => $sender['email'],
                'phone'               => $sender['phone'],
                'company_name'        => $sender['company_name'],
                'company_phone'       => $sender['company_phone'],
                'position'            => $sender['position'],
                'region'              => RefOrderRegion::where('id', $sender['region'])->get()->pluck('name')->first(),
                'eligible_to_contact' => RefContactHour::where('id', $sender['eligible_to_contact'])->get()->pluck('name')->first(),
                'service_needed'      => RefServiceNeeded::where('id', $sender['service_needed'])->get()->pluck('name')->first(),
                'order'               => RefOrderServices::where('id', $sender['ref_service_id'])->get()->pluck('name')->first(),
                'created_at'          => $sender['created_at'],
                'mail_to'             => $mail_to,
                'mail_fullname'       => $this->property('mail_fullname'),
                'our_whatsapp'        => Settings::get('whatsapp'),
            ];

            Mail::send('mpmrent::mail.admin_order', $vars, function($message) use ($vars) {
                $message->to($vars['mail_to'], $vars['mail_fullname']);
                $message->bcc('contact@mpm-rent.com');
                
                $message->data = $vars;
            });

            Mail::send('mpmrent::mail.admin_orderauto', $vars, function($message) use ($vars) {
                $message->subject("[MPMRent #".$vars['order_number']."] (".$vars['name']."), Thank You for Your Request!");
                $message->to($vars['email'], $vars['mail_fullname']);
                
                $message->data = $vars;
            });
            Flash::success('Mail has been sent !');
        } catch (\Exception $e) {
            // and throw the error again.
            Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }
    }
}