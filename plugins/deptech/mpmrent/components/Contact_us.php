<?php namespace Deptech\MPMRent\Components;

use Cms\Classes\ComponentBase;
use RainLab\Translate\Classes\Translator;
use Deptech\MPMRent\Models\ContactUsModel;
use Deptech\MPMRent\Models\SubjectMailModel;
use Deptech\MPMRent\Models\SubjectCategoryModel;

use DB;
use Mail;
use Input;
use Flash;
use Redirect;

class Contact_us extends ComponentBase
{

    protected $translator;

    public function componentDetails()
    {
        return [
            'name'        => 'ContactUs Component',
            'description' => 'ContactUs Form Component'
        ];
    }

    public function defineProperties()
    {
        return [
            'mail_to' => [
                'title' => 'Send email to',
                'description' => 'Send email to',
                'default' => 'marketing@mpm-rent.com',
                'type' => 'string',
                'required' => true,
            ],
            'mail_fullname' => [
                'title' => 'Sender fullname',
                'description' => 'Sender fullname',
                'default' => 'Marketing',
                'type' => 'string',
                'required' => true,
            ],
        ];
    }

    // public function init()
    // {
    //     $this->addComponent('Alxy\Captcha\Components\Captcha', 'Captcha', []);
    // }
    
    public function onRun() {
        $this->page['subjects'] = $this->getSubject();
    }
    
    public function getProperty($propertyName) { 
        return $this->property($propertyName); 
    }
    public function getSubject() {

        $subject_list = SubjectCategoryModel::with('subject')->has('subject')->where([
            ['deleted_at', NULL],
        ])
        ->orderBy('sort')
        ->get();

        return $subject_list;
    }
    public function onSubmitContact() {

        try {

            // Begin a transaction
            DB::beginTransaction();

            // catch post value
            $post = Input::all();
            $contact = new ContactUsModel;
            
            $contact->name         = $post['name'];
            $contact->email        = $post['email'];
            $contact->phone_number = $post['phone_number'];
            $contact->message      = $post['message'];
            $contact->subject      = $post['subject'];
            $contact->company_name = $post['company_name'];

            $contact->save();

            $id = $contact->id;

            DB::commit();

            $this->sendEmailContact($post);
            $this->sendEmailAutoReply($post);

            Flash::success('Email successfully sent.');
            
            // $this->translator = Translator::instance();
            // return Redirect::to($this->translator->getLocale().'/contact');
            $this->translator = Translator::instance();
            return Redirect::to($this->translator->getLocale().'/contact/thanks/'. $id);

        } catch (\Exception $e) {
            // An error occured; cancel the transaction...
            DB::rollback();

            // and throw the error again.
            throw $e;
            Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }

    }

    protected function sendEmailAutoReply(Array $sender) {

        try {
            /* Set the $timezone */
            date_default_timezone_set("Asia/Bangkok");
            $time = date("H");

            $time_greet = '';
            /* If the time is less than 1200 hours, show good morning */
            if ($time < "12") {
                $time_greet = "Pagi";
            } else
            /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
            if ($time >= "12" && $time < "17") {
                $time_greet = "Siang";
            } else
            /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
            if ($time >= "17" && $time < "19") {
                $time_greet = "Sore";
            } else
            /* Finally, show good night if the time is greater than or equal to 1900 hours */
            if ($time >= "19") {
                $time_greet = "Malam";
            }

            $mail_to =  $sender['email'];
            $vars = [
                'fullname'      => $sender['name'],
                'email'         => $sender['email'],
                'timegreet'     => $time_greet,
                'mail_to'       => $mail_to,
                'mail_fullname' => 'no-reply',
                'subject'       => 'no-reply'
            ];

            Mail::send('mpmrent::contact.autoreply', $vars, function($message) use ($vars) {
                $message->to($vars['mail_to'], $vars['mail_fullname']);
                // $message->subject($vars['subject']);
                $message->data = $vars;
            });
            Flash::success('Mail has been sent !');

        } catch (\Exception $e) {
            // and throw the error again.
            // Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }
    }

    protected function sendEmailContact(Array $sender) {
        try {
            $mail_to = ($this->property('mail_to')) ? $this->property('mail_to') : 'contact@mpm-rent.com';
            $vars = [
                'fullname'        => $sender['name'],
                'email'           => $sender['email'],
                'phone_number'    => $sender['phone_number'],
                'company_name'    => $sender['company_name'],
                'subject'         => $sender['subject'],
                'contact_message' => $sender['message'],
                'mail_to'         => $mail_to,
                'mail_fullname'   => $this->property('mail_fullname')
            ];

            Mail::send('mpmrent::contact.admin', $vars, function($message) use ($vars) {
                $message->to($vars['mail_to'], $vars['mail_fullname']);
                
                $message->subject($vars['subject']);
                $message->data = $vars;
            });
            Flash::success('Mail has been sent !');
        } catch (\Exception $e) {
            // and throw the error again.
            // Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }
    }
}
