<?php

namespace Deptech\MPMRent\Components;

use Deptech\MPMRent\Models\SliderModel;
use Request;

class Slider extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
    {
        return [
            'name' => 'Slider',
            'description' => 'Slider for MPM Rent'
        ];
    }


    public function onRun()
    {
        $this->page['sliders'] = $this->getSlider();
    }

    /**
     * [getSlider get list slider data]
     * @return object slider data
     */
    public function getSlider() {
        $slider_lists = SliderModel::where([
            ['deleted_at', NULL],
            ['is_active', 1]
        ])->orderBy('sort', 'ASC')->get(['id','title', 'description', 'images']);

        return $slider_lists;
    }
}