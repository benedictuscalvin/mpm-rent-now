<?php

namespace Deptech\MPMRent\Components;

use Deptech\MPMRent\Models\OfficeLocationModel;
use Request;

class Map extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Map',
            'description' => 'A Map to discover where we available'
        ];
    }

    public function defineProperties()
    {
        return [
            'mapfieldId' => [
                'title' => 'Element ID',
                'description' => 'Element ID',
                'default' => 'map',
                'type' => 'string',
                'required' => true,
            ],
            'height' => [
                'title' => 'Height',
                'description' => 'Set Height Map',
                'default' => '400px',
                'type' => 'string',
                'required' => true,
            ],
            'latitude' => [
                'title' => 'Latitude',
                'description' => 'Set Latitude Map',
                'default' => -0.176,
                'type' => 'string',
                'required' => true,
            ],
            'longitude' => [
                'title' => 'Longtitude',
                'description' => 'Set Longtitude Map',
                'default' => 119.268,
                'type' => 'string',
                'required' => true,
            ],
            'zoom' => [
                'title' => 'Zoom',
                'description' => 'Set Zoom Value for map',
                'default' => 3,
                'type' => 'string',
                'required' => true,
            ],
            'maxZoom' => [
                'title' => 'Max Zoom',
                'description' => 'Set Max Zoom Value for map',
                'default' => 18,
                'type' => 'string',
                'required' => true,
            ]
        ];
    }

    public function getMapOptions()
    {
        return Maps::orderBy('name')->lists('name', 'id');
    }

    public function onRun()
    {
        $this->page['map'] = $this->map();
        $this->page['addresses'] = $this->getAddress();
        $this->page['list_city'] = $this->getListCity();
        $this->page['count_branches'] = OfficeLocationModel::where('deleted_at', NULL)->where('type', 2)->count();
        $this->page['count_services'] = OfficeLocationModel::where('deleted_at', NULL)->where('type', 3)->count();
        $this->addJs('/plugins/deptech/mpmrent/assets/leaflet/leaflet.js');
        $this->addJs('/plugins/deptech/mpmrent/assets/leaflet-boundary-canvas/src/BoundaryCanvas.js');
        $this->addCss('/plugins/deptech/mpmrent/assets/leaflet/leaflet.css');
    }


    /**
     * this for get map from leaflet.
     */
    public function map()
    {
        $map = OfficeLocationModel::where('deleted_at', NULL)->get();
        $map->mapfieldId = $this->property('mapfieldId');
        $map->latitude = $this->property('latitude');
        $map->longitude = $this->property('longitude');
        $map->zoom = $this->property('zoom');
        $map->maxZoom = $this->property('maxZoom');
        $map->height = $this->property('height');
        $map->objectsToDisplay = OfficeLocationModel::where('deleted_at', NULL)->orderBy('type', 'DESC')->get();

        $showOnlyObject = $this->property('showOnlyObject');

        return $map;
    }


    /**
     * Get office address 
     */
    public function getAddress() {
        $address = $slider_lists = OfficeLocationModel::where([
            ['deleted_at', NULL],
            ['is_publish', 1]
        ])->whereIn('type', array(1, 2))
        ->orderBy('id', 'ASC')->get(['id','name','address', 'city']);
        return $address;
    }
    
    public function getListCity() {
        $city = $slider_lists = OfficeLocationModel::where([
            ['deleted_at', NULL],
            ['is_publish', 1]
        ])->orderBy('id', 'ASC')->pluck('city')->unique()->toArray();

        $str_city = implode (", ", $city);

        return $str_city;
    }

    protected function mapQuery()
    {
        return OfficeLocationModel::where('deleted_at', NULL);
    }
}