<?php
namespace Deptech\MPMRent\Components;

use Deptech\MPMRent\Models\TestimonialModel;
use Request;

class Testimonial extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
    {
        return [
            'name' => 'Testimonial',
            'description' => 'Testimonial for MPM Rent'
        ];
    }

    public function onRun() {
    	$this->page['testimonials'] = $this->getTestimonial();
    }

    public function getTestimonial() {
    	$testimonial = TestimonialModel::where([
            ['deleted_at', NULL],
            ['is_publish', 1]
        ])->orderBy('id', 'ASC')->get(['id','name', 'position', 'company', 'message','images']);

        return $testimonial;
    }
}