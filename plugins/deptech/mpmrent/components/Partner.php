<?php
namespace Deptech\MPMRent\Components;

use Deptech\MPMRent\Models\PartnerModel;
use Request;

class Partner extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
    {
        return [
            'name' => 'Partner',
            'description' => 'List Partner on MPM Rent'
        ];
    }

    public function onRun() {
    	$this->page['partners'] = $this->getPartner();
    }

    public function getPartner() {
    	$partners = PartnerModel::where([
            ['deleted_at', NULL],
            ['is_active', 1]
        ])->orderBy('id', 'ASC')->get(['id','company_name', 'images', 'link']);

        return $partners;
    }
}