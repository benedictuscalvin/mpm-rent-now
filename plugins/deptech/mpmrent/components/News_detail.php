<?php namespace Deptech\MPMRent\Components;

use Lang;
use Redirect;
use BackendAuth;
use Deptech\MPMRent\Models\NewsModel;
use RainLab\Translate\Classes\Translator;

class News_detail extends \Cms\Classes\ComponentBase
{

	/**
     * @var The post model used for display.
     */
    public $post;

	public function componentDetails()
    {
        return [
            'name'        => 'Post detail News',
            'description' => 'A Post detail'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'Slug',
                'description' => 'A slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
        ];
    }

    public function onRun()
    {
        $translator = Translator::instance();
        $activeLocale = $translator->getLocale();
        if ($activeLocale == 'jp') {
            return Redirect::to('/jcbu');
        }
		$this->post        = $this->page['post'] = $this->loadPost();
		$this->lastestNews = $this->page['lastestNews'] = $this->loadOtherNews();
        $this->addJs('/plugins/deptech/mpmrent/assets/sharing-button.js');
    }

    public function onRender()
    {
        if (empty($this->post)) {
            $this->post = $this->page['post'] = $this->loadPost();
        }

        if (empty($this->lastestNews)) {
            $this->lastestNews = $this->page['lastestNews'] = $this->loadOtherNews();
        }
    }


    /**
     * Load post
     * @return object Detail news
     */
    protected function loadPost()
    {
    	$slug = $this->property('slug');

    	$post = new NewsModel;

    	$post = $post->where('slug', $slug);

    	try {
            $post = $post->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            $this->setStatusCode(404);
            return $this->controller->run('404');
        }

        return $post;
    }

    /**
     * [loadOtherNews description]
     * @return object data to show lastest news 
     */
    protected function loadOtherNews() {
    	$lastest_post = NewsModel::orderBy('id', 'desc')->take(4)->get();
    	/*
         * Add a "url" helper attribute for linking to each post and category
         */
        $lastest_post->each(function($post) {
            $post->setUrl($this->postPage, $this->controller);
        });
        
    	return $lastest_post;
    }
}