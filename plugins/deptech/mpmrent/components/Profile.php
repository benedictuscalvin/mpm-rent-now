<?php
namespace Deptech\MPMRent\Components;

use Deptech\MPMRent\Models\ProfileModel;
use Request;

class Profile extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
    {
        return [
            'name' => 'Profile Slider',
            'description' => 'Profile Slider for MPM Rent'
        ];
    }

    public function onRun() {
    	$this->page['profiles'] = $this->getTestimonial();
    }

    public function getTestimonial() {
    	$profile = ProfileModel::where([
            ['deleted_at', NULL],
            ['is_publish', 1]
        ])->orderBy('id', 'ASC')->get(['id','name', 'position', 'description','images']);

        return $profile;
    }
}