<?php
namespace Deptech\MPMRent\Components;

use Deptech\MPMRent\Models\Rating as RatingModel;
use Redirect;
use DB;
use Response;
use Input;
use Flash;
use Carbon\Carbon;

class Rating extends \Cms\Classes\ComponentBase
{

	public function componentDetails()
    {
        return [
            'name' => 'Rating',
            'description' => 'Counter Rating'
        ];
    }

    public function defineProperties()
    {
        return [
            'rate' => [
                'title'       => 'Rate',
                'description' => 'Rating from email',
                'default'     => '{{ :rate }}',
                'type'        => 'string',
            ],
        ];
    }

	public function onRun()
    {
        // Begin a transaction
        // DB::beginTransaction();

        // // catch post value
        // $post = Input::all();

        // $rate              = new RatingModel;
        // $rate->message     = (Input::get("messages")) ? Input::get("messages") : '';
        // $rate->rating      = (Input::get("rating")) ? Input::get("rating") : 0;
        // $rate->created_at = Carbon::now();
        // $rate->save();

        // DB::commit();

        // Flash::success('Feedback submitted.');
        // return Redirect::to('/');
    }

    public function onSubmit() {

        try {
            // Begin a transaction
            DB::beginTransaction();

            // catch post value
            $post = Input::all();

			$rate              = new RatingModel;
			$rate->message     = ($post['message']) ? $post['message'] : '';
			$rate->rating      = ($post['rating']) ? $post['rating'] : 0;
			$rate->created_at  = Carbon::now();
        	$rate->save();

            DB::commit();

            setcookie("is_feedback", 1, time()+31556926 ,'/');

            Flash::success('Feedback submitted.');
           	return Redirect::refresh();
        } catch (\Exception $e) {
            // An error occured; cancel the transaction...
            DB::rollback();

            // and throw the error again.
            throw $e;
            // Flash::error($e->getMessage());
            Flash::error('Unable to continue proses.');
        }

    }

    public function setCookie($value, $name){
      $minutes = 2560;
      $response = new Response('Set Cookie');
      $response->withCookie(cookie($name, $value, $minutes));
      return $response;
   }
}