<?php
namespace Deptech\MPMRent\Components;

use Deptech\MPMRent\Models\ClientsModel;
use Request;

class Clients extends \Cms\Classes\ComponentBase
{
	public function componentDetails()
    {
        return [
            'name' => 'Clients',
            'description' => 'List Clients on MPM Rent'
        ];
    }

    public function onRun() {
    	$this->page['clients'] = $this->getClients();
    }

    public function getClients() {
    	$partners = ClientsModel::where([
            ['deleted_at', NULL],
            ['is_active', 1]
        ])->orderBy('sort', 'ASC')->orderBy('id', 'ASC')->get(['id','name', 'image', 'type']);

        return $partners;
    }
}