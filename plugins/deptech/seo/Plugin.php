<?php namespace Deptech\Seo;

use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Deptech\Seo\Helpers\Helpers as ProjectHelpers;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
            'Deptech\Seo\Components\SeoNews'            => 'seo_news',
            'Deptech\Seo\Components\SeoSettings'        => 'seo_settings',
            'Deptech\Seo\Components\SeoStatic'          => 'seo_static',
        ];
    }

    public function registerSettings()
    {
    	 return [
            'settings' => [
                'label'       => 'SEO Settings',
                'description' => 'Settings SEO for MPM website',
                'icon'        => 'icon-search',
                'category'    =>  SettingsManager::CATEGORY_MYSETTINGS,
                'class'       => 'Deptech\Seo\Models\Settings',
                'order'       => 100
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'strftime' => function(Carbon $time, $format) {
                   return strftime($format, $time->getTimestamp());
                },
                'generateTitle'        => [$this, 'generateTitle'],
                'generateCanonicalUrl' => [$this, 'generateCanonicalUrl'],
                'otherMetaTags'        => [$this, 'otherMetaTags'],
                'generateOgTags'       => [$this, 'generateOgTags'],
                'generateTwitterTags'  => [$this, 'generateTwitterTags']
            ],
       ];
    }

    public function generateOgTags($post)
    {
        $helper = new ProjectHelpers();

        $ogMetaTags = $helper->generateOgMetaTags($post);
        return $ogMetaTags;
    }

    public function generateTwitterTags($post)
    {
        $helper = new ProjectHelpers();

        $twitterMetaTags = $helper->generateTwitterTags($post);
        return $twitterMetaTags;
    }

    public function otherMetaTags()
    {
        $helper = new ProjectHelpers();

        $otherMetaTags = $helper->otherMetaTags();
        return $otherMetaTags;
    }

    public function generateTitle($title)
    {
        $helper = new ProjectHelpers();
        $title = $helper->generateTitle($title);
        return $title;
    }

    public function generateCanonicalUrl($url)
    {
        $helper = new ProjectHelpers();
        $canonicalUrl = $helper->generateCanonicalUrl();
        return $canonicalUrl;
    }
}
